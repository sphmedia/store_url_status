import yaml
from asyncio.log import logger
import threading as thr
import pandas as pd
from datetime import datetime
from helpers.boto3connection import Boto3connection
from helpers.s3download import S3GetObject
from helpers.readcsv import Readcsv
from helpers.batchwriteitem import BatchWriteItem


def read_config(file_path):
    '''
    Task read the config file and return values
    parameters
    ----------
    file_path: file path which is need to read
    '''
    with open(file_path) as stream:
        try:
            data = yaml.safe_load(stream)
            return data

        except yaml.YAMLError as e:
            logger.info(e)


def s3_get_file(client, bucket, key):
    '''
    Task get object from s3 bucket based on the data format
    parameters
    ----------
    client: s3 client
    bucket: s3 bucket name
    key: file sub path
    file_type : json or csv
    '''

    responce = S3GetObject.s3_get_object(
        client, bucket, key)

    try:
        return responce
    except Exception as e:
        logger.info(e)


def main():

    start_time = datetime.now()

    confgs = read_config("helpers/configs/config.yaml")

    region = confgs["region"]
    s3_bucket = confgs["s3_bucket"]
    s3_key = confgs["s3_key"]
    chunksize = confgs["chunksize"]
    db_table = confgs["db_table"]
    print(db_table)
    s3client = Boto3connection.get_connection(region, 's3')

    url_info = s3_get_file(
        s3client, s3_bucket, s3_key)
    url_info = Readcsv.readcsv(url_info, chunksize)

    dynamodb = Boto3connection.get_connection(region, 'dynamodb')

    threads = []
    items = []

    rdc = Readcsv()

    for rows in url_info:
        print(rows)
        df = pd.DataFrame(rows)

        df = df.reset_index()

        t = thr.Thread(target=rdc.orchestrate_data,
                       args=(df, items, dynamodb, db_table))
        t.start()
        threads.append(t)

    for thread in threads:
        thread.join()

    end_time = datetime.now()

    print("time ", (end_time - start_time))


if __name__ == main():
    main()
