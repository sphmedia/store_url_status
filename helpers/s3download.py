from asyncio.log import logger
from http import client
import boto3


class S3GetObject:

    def s3_get_object(connection, bucket, key):
        try:
            response = connection.get_object(Bucket=bucket, Key=key)

            content = response['Body']

            return content

        except Exception as e:
            logger.info(e)
