import calendar
import imp
import pandas as pd
import requests
from asyncio.log import logger
from datetime import datetime


class Readcsv:

    def readcsv(file_path, chunck_size):

        try:
            chunck = pd.read_csv(file_path, chunksize=chunck_size)
            return chunck
        except FileNotFoundError as e:
            logger.info(e)

    def dynamodb_insert(self, connection, table, items):

        print(items["PutRequest"]["Item"])

        try:

            response = connection.put_item(
                TableName=table,
                Item=items["PutRequest"]["Item"]
            )

        except KeyError as e:
            logger.info(e)

    def orchestrate_data(self, df, items, dbconnection, db_table):
        '''
        Task orchestrate data read by csv and format to response
        parameters
        -----------------------
        campain_data: csv file name
        '''

        for index, row in df.iterrows():
            # i = 0
            name = row["name"]
            url = row["url"]

            try:
                current_time = calendar.timegm(datetime.now().timetuple())
                r = requests.head(url)

                PutRequest = {'PutRequest': {'Item': {}}}

                PutRequest["PutRequest"]["Item"] = {"time": {'N': str(current_time)}, "name": {
                    'S': str(row['name'])}, "url": {'S': str(row['url'])}, "status": {'S': str(r.status_code)}}

                self.dynamodb_insert(dbconnection, db_table, PutRequest)

            except requests.exceptions.RequestException as e:
                PutRequest = {'PutRequest': {'Item': {}}}

                PutRequest["PutRequest"]["Item"] = {"time": {'N': str(current_time)}, "name": {
                    'S': str(row['name'])}, "url": {'S': str(row['url'])}, "status": {'S': str(404)}}

                self.dynamodb_insert(dbconnection, db_table, PutRequest)

                items.append(PutRequest)
                logger.info(e)

        return items

    # def orchestrate_campain_data(df, items):
    #     '''
    #     Task orchestrate data read by csv and format to response
    #     parameters
    #     -----------------------
    #     campain_data: csv file name
    #     '''

    #     current_time = datetime.now()

    #     for index, row in df.iterrows():
    #         # i = 0
    #         name = row["name"]
    #         url = row["url"]

    #         try:
    #             r = requests.head(url)

    #             response = {"data": {}}

    #             response["data"] = {"name": name,
    #                                 "url": url, "status_code": r.status_code}

    #             items.append(response)

    #         except requests.exceptions.RequestException as e:
    #             response = {"data": {}}

    #             response["data"] = {"name": name,
    #                                 "url": url, "status_code": "404"}

    #             items.append(response)
    #             logger.info(e)

    #     return items
