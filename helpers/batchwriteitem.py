from asyncio.log import logger
import boto3


class BatchWriteItem:

    def batch_write(connection, table, items):

        try:

            response = connection.put_item(
                TableName = table,
                Item = items
            )

        except KeyError as e:
            logger.info(e)