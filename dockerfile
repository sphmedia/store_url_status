FROM python:3.8-slim-buster 
LABEL Anushka <uyanushka@gmail.com>

# upgrade pip
RUN python -m pip install --upgrade pip

# add user, use and set WORKDIR
RUN adduser --disabled-password --gecos "" appuser
USER appuser
WORKDIR /home/appuser

# add user bin to path
ENV PATH="/home/appuser/.local/bin:${PATH}"

# copy artifacts
COPY --chown=appuser:appuser helpers ./helpers
COPY --chown=appuser:appuser app.py .
COPY --chown=appuser:appuser requirements.txt .

# install required pip mpdules
RUN pip install --user -r requirements.txt

CMD ["python3", "app.py"]
